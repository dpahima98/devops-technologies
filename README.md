# DevOps Documentation Repository

Welcome to the DevOps Documentation Repository! This repository provides comprehensive documentation and automation scripts for managing DevOps infrastructures using Kubernetes, monitoring tools, and other essential components.

## Table of Contents

1. [Introduction](#introduction)
2. [Prerequisites](#prerequisites)
   - [Common Prerequisites](#common-prerequisites)
   - [Linux](#linux)
   - [Windows](#windows)
   - [Mac](#mac)
   - [Development Tools (Optional)](#development-tools-optional)
3. [Docker](./docker/README.md)
4. [Kubernetes](./kubernetes/README.md)
    - [Overview](./kubernetes/README.md#overview)
    - [Installation](./kubernetes/README.md#installation)
5. [License](#license)

## Introduction

This repository serves as a centralized hub for DevOps documentation and automation scripts. Whether you are new to DevOps or an experienced practitioner, you will find resources here to guide you through the setup and management of critical components such as Kubernetes and monitoring tools.

## Prerequisites

Before you begin, make sure you have the following prerequisites:

### Common Prerequisites:

- **Git:**
   - [Install Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

- **Docker:**
   - [Install Docker](https://docs.docker.com/get-docker/)

### Linux:

- **Package Manager:**
   - Ensure that your Linux distribution has an updated package manager (e.g., `apt` for Debian/Ubuntu, `yum` for Red Hat/Fedora).

### Windows:

- **Windows Subsystem for Linux (WSL):**
   - [Install WSL](https://docs.microsoft.com/en-us/windows/wsl/install)

- **Docker Desktop for Windows:**
   - [Install Docker Desktop for Windows](https://docs.docker.com/desktop/install/windows-install/)

### Mac:

- **Homebrew (Package Manager for Mac):**
   - [Install Homebrew](https://brew.sh/)
### Development Tools (Optional):

- **Shell Environment (Optional):**
    - Set up your preferred shell environment, such as Bash or Zsh or xonsh, with any custom configurations you prefer.

 - **Code Editors (Optional):**
    - Install code editors like Visual Studio Code or Pycharm for script editing.


Now, explore the specific documentation and enjoy :

- [Docker Documentation](./docker/README.md)
- [Kubernetes Documentation](./kubernetes/README.md)

## License

This project is licensed under the [GNU General Public License v3.0](LICENSE.md). 
