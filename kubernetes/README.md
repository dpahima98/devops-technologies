# Kubernetes Documentation

# Table of Contents

1. [Overview](#overview)
   - 1.1 [Why Kubernetes?](#why-kubernetes)

2. [Requirements](#requirements)
   - 2.1 [Docker](#docker)
   - 2.2 [kubectl](#kubectl)

3. [Additional Tools](#additional-tools)
   - 3.1 [kubens](#kubens)
      - 3.1.1 [Installation](#installation)
      - 3.1.2 [Usage](#usage)
   - 3.2 [kubectx](#kubectx)
      - 3.2.1 [Installation](#installation-1)
      - 3.2.2 [Usage](#usage-1)
   - 3.3 [k9s](#k9s)
      - 3.3.1 [Installation](#installation-2)
      - 3.3.2 [Usage](#usage-2)

4. [Installation](#installation-3)
   - 4.1 [Creating a Cluster using k3d](#creating-a-cluster-using-k3d)


## Overview

Kubernetes is a powerful open-source container orchestration platform that automates the deployment, scaling, and management of containerized applications. It simplifies the deployment and operation of applications by providing robust mechanisms for handling the complexities of containerized environments.

### Why Kubernetes?

- **Scalability:** Easily scale applications up or down based on demand.
- **Resilience:** Ensure high availability and fault tolerance for applications.
- **Portability:** Run applications consistently across different environments.
- **Resource Efficiency:** Optimize resource utilization through efficient container management.
- **Automation:** Automate deployment, scaling, and operational tasks.

## Requirements

Before you begin with Kubernetes, ensure that the following prerequisites are met:

- **Docker:** Install [Docker](https://docs.docker.com/get-docker/) to create, deploy, and run containers.
- **kubectl:** Install [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) for interacting with Kubernetes clusters.

## Additional Tools

Enhance your Kubernetes experience with the following tools:

### kubens

`kubens` is a utility that allows you to switch between Kubernetes namespaces easily.

#### Installation:

```bash
# Linux
sudo curl -o /usr/local/bin/kubens https://raw.githubusercontent.com/ahmetb/kubectx/master/kubens
sudo chmod +x /usr/local/bin/kubens

# macOS
brew install kubectx
```

#### Usage:

```bash
kubens <namespace-name>
```

### kubectx

`kubectx` is a utility that allows you to switch between Kubernetes clusters.

#### Installation:

```bash
# Linux
sudo curl -o /usr/local/bin/kubectx https://raw.githubusercontent.com/ahmetb/kubectx/master/kubectx
sudo chmod +x /usr/local/bin/kubectx

# macOS
brew install kubectx
```

#### Usage:

```bash
kubectx <context-name>
```

### k9s

`k9s` is a terminal-based Kubernetes CLI that provides a visual representation of your Kubernetes clusters. [More Info](https://github.com/derailed/k9s)

#### Installation:

```bash
# Linux
brew install k9s

# macOS
brew install k9s
```

#### Usage:

```bash
k9s
```

## Installation

### Creating a Cluster using k3d

[k3d](https://k3d.io/v5.6.0/) is a lightweight wrapper to run Kubernetes clusters in Docker.

1. **Install k3d:** Download and install k3d by following the instructions [here](https://k3d.io/v5.6.0/#releases).
2. **Create a Cluster:** Use the following command to create a Kubernetes cluster named "mycluster".

```bash
k3d cluster create mycluster
```
3. **Access Kubernetes:** Once the cluster is created, you can interact with it using `kubectl`

```bash
kubectl cluster-info --context k3d-mycluster
#This command will show you information about your newly created Kubernetes cluster.
```