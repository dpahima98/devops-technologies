# Docker Documentation

# Table of Contents

- [Containerization Basics](#containerization-basics)
  - [Understanding Containerization](#understanding-containerization)
    - [Benefits](#benefits)
  - [Docker Fundamentals](#docker-fundamentals)
    - [Docker Images](#docker-images)
      - [Definition](#definition)
      - [Creation (Dockerfile)](#creation-dockerfile)
      - [Registry](#registry)
      - [Common Commands](#common-commands)
    - [Docker Containers](#docker-containers)
      - [Definition](#definition-1)
      - [Lifecycle](#lifecycle)
      - [Portability](#portability)
      - [Common Commands](#common-commands-1)
    - [Dockerfile](#dockerfile)
      - [Purpose](#purpose)
      - [Example](#example)
  - [Additional Docker Commands](#additional-docker-commands)
    - [Managing Images](#managing-images)
    - [Managing Containers](#managing-containers)
    - [Networking](#networking)
    - [Volume Management](#volume-management)

## Containerization Basics

### Understanding Containerization

Containerization is a lightweight, portable, and efficient way to encapsulate, distribute, and run software applications. It involves bundling an application and its dependencies into a single container image.

#### Benefits

1. **Isolation:**
   Containers provide process and file system isolation, ensuring that applications run independently of each other.

2. **Portability:**
   Containers encapsulate everything an application needs to run, making it easy to deploy the same application across various environments consistently.

3. **Resource Efficiency:**
   Containers share the host OS kernel, leading to faster startup times and reduced resource overhead compared to traditional virtual machines.

4. **Scalability:**
   Containerized applications can be easily scaled up or down to handle varying workloads, providing flexibility and responsiveness.

5. **Ease of Deployment:**
   Containers simplify the deployment process, as the entire application, along with its dependencies, is packaged into a single container image.

6. **Version Control:**
   Container images can be versioned, making it simple to roll back to a previous version or deploy multiple versions simultaneously.

### Docker Fundamentals

#### Docker Images

- **Definition:**
  An image is a lightweight, standalone, and executable package that includes everything needed to run a piece of software.

- **Creation (Dockerfile):**
  Images are created using a Dockerfile, a text file specifying the base image, application code, dependencies, and configuration settings.

- **Registry:**
  Docker images can be stored and shared through a container registry such as Docker Hub or a private registry.

- **Common Commands:**
  - `docker images`: List all locally available images.
  - `docker pull <image-name>:<tag>`: Download a specific image from a registry.

#### Docker Containers

- **Definition:**
  A container is a running instance of a Docker image, representing a lightweight, isolated environment for running an application.

- **Lifecycle:**
  Containers can be created, started, stopped, moved, and deleted. They ensure consistency across different environments.

- **Portability:**
  Containers run consistently on any system that supports Docker, promoting the "Build Once, Run Anywhere" philosophy.

- **Common Commands:**
  - `docker ps`: List all running containers.
  - `docker ps -a`: List all containers, including stopped ones.
  - `docker run <image-name>`: Create and start a new container from an image.
  - `docker exec -it <container-id> /bin/bash`: Access the shell inside a running container.

#### Dockerfile

- **Purpose:**
  A Dockerfile is a script that contains instructions for building a Docker image. It defines the base image, sets up the environment, copies application code, installs dependencies, and configures runtime settings.

- **Example:**
  ```Dockerfile
  # Use an official Node.js runtime as a base image
  FROM node:14

  # Set the working directory
  WORKDIR /usr/src/app

  # Copy package.json and package-lock.json to the working directory
  COPY package*.json ./

  # Install dependencies
  RUN npm install

  # Copy application code to the working directory
  COPY . .

  # Expose a port
  EXPOSE 8080

  # Command to run the application
  CMD ["node", "app.js"]

#### Additional Docker Commands

- **Managing Images:**
  - `docker rmi <image-id>`: Remove a Docker image.
  - `docker image prune`: Remove all dangling images.
- **Managing Containers:**
  - `docker stop <container-id>`: Stop a running container.
  - `docker rm <container-id>`: Remove a stopped container.
  - `docker container prune`: Remove all stopped containers.
- **Networking:**
  - `docker network ls`: List all Docker networks.
  - `docker network inspect <network-name>`: Display detailed information about a network.
- **Volume Management:**
  - `docker volume ls`: List all Docker volumes.
  - `docker volume create <volume-name>`: Create a new Docker volume.
